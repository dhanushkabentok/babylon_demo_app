package com.sampathsampel.babylon.sampelapplication.network;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import com.sampathsampel.babylon.sampelapplication.d.ApplicationContext;
import com.sampathsampel.babylon.sampelapplication.d.ApplicationScope;
import com.sampathsampel.babylon.sampelapplication.d.module.ContextModule;

/**
 * Created by dhanushka on 12/10/2017.
 */
@Module(includes = {ContextModule.class})
public class UserNetworkCall {


    @Provides
    @ApplicationScope
    public Context apiService(@ApplicationContext Context context) {

        return context;
    }


}
