package com.sampathsampel.babylon.sampelapplication.network;

import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

import com.sampathsampel.babylon.sampelapplication.helpers.NetworkAccess;
import com.sampathsampel.babylon.sampelapplication.network.model.GithubRepo;
import com.sampathsampel.babylon.sampelapplication.network.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dhanushka on 19/10/2017.
 */

public class CallApi {

    @Inject
    ApiService mSicModul;
    private Context mContext;



    public CallApi(Context context, ApiService m) {
        mContext = context;
        mSicModul = m;
    }

    public void getServercal(final getUserRegisterCallback callback, User mUser) {

        if (!NetworkAccess.isNetworkAvailable()) {
            callback.onFailedUserRegister(null);
            return;
        }



        mSicModul.getAllRepos(mUser).enqueue(new Callback<GithubRepo>() {
            @Override
            public void onResponse(Call<GithubRepo> call, Response<GithubRepo> response) {
                Log.d("rgfgfg", "gfgghrtgrthw");
                if (response.isSuccessful()) {
                    callback.onSuccessUserRegister(response.body().getUser_token());
                }

            }

            @Override
            public void onFailure(Call<GithubRepo> call, Throwable t) {
                callback.onFailedUserRegister(t.getMessage());
            }
        });}


    public void register(final GetLogingCallBack callback, User mUser) {

        if (!NetworkAccess.isNetworkAvailable()) {
            callback.onFailedLogingCallBack(null);
            return;
        }


        mSicModul.create_user(mUser).enqueue(new Callback<GithubRepo>() {
            @Override
            public void onResponse(Call<GithubRepo> call, Response<GithubRepo> response) {
                Log.d("rgfgfg", "gfgghrtgrthw");
                callback.onSuccessLogingCallBack("susses");

            }

            @Override
            public void onFailure(Call<GithubRepo> call, Throwable t) {
                callback.onFailedLogingCallBack(t.getMessage());
            }
        });
    }

    public interface getUserRegisterCallback {
        void onSuccessUserRegister(String sdpList);

        void onFailedUserRegister(String message);
    }

    public interface GetLogingCallBack {
        void onSuccessLogingCallBack(String sdpList);

        void onFailedLogingCallBack(String message);
    }
}
