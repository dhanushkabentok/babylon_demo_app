package com.sampathsampel.babylon.sampelapplication.d.component;


import com.squareup.picasso.Picasso;

import dagger.Component;
import com.sampathsampel.babylon.sampelapplication.d.ApplicationScope;
import com.sampathsampel.babylon.sampelapplication.d.module.ActivityModule;
import com.sampathsampel.babylon.sampelapplication.d.module.ApplicarionServicemodule;
import com.sampathsampel.babylon.sampelapplication.d.module.NetworkAccessModule;
import com.sampathsampel.babylon.sampelapplication.d.module.PicassoModule;
import com.sampathsampel.babylon.sampelapplication.d.module.SicModul;
import com.sampathsampel.babylon.sampelapplication.helpers.NetworkAccess;
import com.sampathsampel.babylon.sampelapplication.network.ApiService;
import com.sampathsampel.babylon.sampelapplication.network.CallApi;

/**
 * Created by dhanushka on 10/10/2017.
 */

@ApplicationScope
@Component(modules = {ApplicarionServicemodule.class, PicassoModule.class, ActivityModule.class, SicModul.class, NetworkAccessModule.class})
public interface ApplicationComponentSampathBank {

    Picasso getPicasso();
    ApiService getApplicatonService();
    CallApi getClasesink();
    NetworkAccess getNetworkAccess();
}
