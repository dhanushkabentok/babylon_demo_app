package com.sampathsampel.babylon.sampelapplication.d.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import com.sampathsampel.babylon.sampelapplication.d.ApplicationContext;
import com.sampathsampel.babylon.sampelapplication.d.ApplicationScope;
import com.sampathsampel.babylon.sampelapplication.network.ApiService;
import com.sampathsampel.babylon.sampelapplication.network.CallApi;

/**
 * Created by dhanushka on 24/10/2017.
 */
@Module(includes = {ContextModule.class, ApplicarionServicemodule.class})
public class SicModul {

    @Provides
    @ApplicationScope
    public CallApi mCallApi(@ApplicationContext Context context, ApiService mApiService) {
        return new CallApi(context, mApiService);
    }
}
