package com.sampathsampel.babylon.sampelapplication;

import android.app.Activity;
import android.app.Application;

import com.sampathsampel.babylon.sampelapplication.d.component.DaggerApplicationComponentSampathBank;
import com.squareup.picasso.Picasso;

import com.sampathsampel.babylon.sampelapplication.d.component.ApplicationComponentSampathBank;

import com.sampathsampel.babylon.sampelapplication.d.module.ContextModule;
import com.sampathsampel.babylon.sampelapplication.helpers.NetworkAccess;
import com.sampathsampel.babylon.sampelapplication.network.ApiService;
import com.sampathsampel.babylon.sampelapplication.network.CallApi;

/**
 * Created by dhanushka on 10/10/2017.
 */

public class ProjectApplication extends Application {
    private ApplicationComponentSampathBank component;
    private ApiService apiService;
    private Picasso picasso;
    private CallApi mCallApi;
    private NetworkAccess mNetworkAccess;

    public static ProjectApplication get(Activity activity) {
        return (ProjectApplication) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponentSampathBank.builder()
                .contextModule(new ContextModule(this))
                .build();

        apiService = component.getApplicatonService();
        picasso = component.getPicasso();
        mCallApi = component.getClasesink();
        mNetworkAccess = component.getNetworkAccess();
    }

    public ApplicationComponentSampathBank getcomponent() {
        return component;
    }
}
