package com.sampathsampel.babylon.sampelapplication.d.component;



import dagger.Component;
import com.sampathsampel.babylon.sampelapplication.d.PerActivity;
import com.sampathsampel.babylon.sampelapplication.d.module.ActivityModule;
import com.sampathsampel.babylon.sampelapplication.view.BaseActivity;


/**
 * Created by dhanushka on 10/10/2017.
 */

@PerActivity
@Component(dependencies = ApplicationComponentSampathBank.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(BaseActivity baseActivity);
}
