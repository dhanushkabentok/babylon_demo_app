package com.sampathsampel.babylon.sampelapplication.network;



import java.util.List;

import com.sampathsampel.babylon.sampelapplication.network.model.GithubRepo;
import com.sampathsampel.babylon.sampelapplication.network.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by dhanushka on 10/10/2017.
 */

public interface ApiService {

    @POST("get_user_token/")
    Call<GithubRepo> getAllRepos(@Body User mUser);

    @POST("create_user/")
    Call<GithubRepo> create_user(@Body User mUser);
}
