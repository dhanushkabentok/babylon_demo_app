package com.sampathsampel.babylon.sampelapplication.d.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import com.sampathsampel.babylon.sampelapplication.d.ApplicationContext;
import com.sampathsampel.babylon.sampelapplication.d.ApplicationScope;
import com.sampathsampel.babylon.sampelapplication.helpers.NetworkAccess;

/**
 * Created by dhanushka on 24/10/2017.
 */
@Module(includes = {ContextModule.class})
public class NetworkAccessModule {

    @Provides
    @ApplicationScope
    public NetworkAccess mNetworkAccess(@ApplicationContext Context context) {
        return new NetworkAccess(context);
    }
}
