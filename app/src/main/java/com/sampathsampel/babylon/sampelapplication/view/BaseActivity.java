package com.sampathsampel.babylon.sampelapplication.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.babylonlabs.babylonlibrary.d.component.ApplicationComponent;
import com.babylonlabs.babylonlibrary.d.component.DaggerApplicationComponent;
import com.sampathsampel.babylon.sampelapplication.d.component.DaggerApplicationComponentSampathBank;
import com.sampathsampel.babylon.sampelapplication.d.module.ContextModule;

@SuppressLint("Registered")
public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    protected ApplicationComponent componentBabylone;
    protected DaggerApplicationComponentSampathBank component;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            getSupportActionBar().hide();
        } catch(NullPointerException e) {}
        component = (DaggerApplicationComponentSampathBank) DaggerApplicationComponentSampathBank.builder()
                .contextModule(new ContextModule(this))
                .build();

        componentBabylone = DaggerApplicationComponent.builder()
                .contextModule(new com.babylonlabs.babylonlibrary.d.module.ContextModule(this))
                .build();
    }

    public void fullscreen() {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
