package com.sampathsampel.babylon.sampelapplication.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sampathsampel.babylon.sampelapplication.R;
import com.sampathsampel.babylon.sampelapplication.network.ApiService;
import com.sampathsampel.babylon.sampelapplication.network.CallApi;

import com.babylonlabs.babylonlibrary.ViewClass.MainAuthenticateClass;
import com.babylonlabs.babylonlibrary.ViewClass.SetupClass;

import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;
import com.babylonlabs.babylonlibrary.helpers.Helpers;
import com.sampathsampel.babylon.sampelapplication.network.model.User;

import javax.inject.Inject;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends BaseActivity implements CallBackInterface.userLogingStatesCallback, CallApi.getUserRegisterCallback, CallApi.GetLogingCallBack {


    private Button btnLogin, btnLinkTo;
    private Button btnLinkToRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    Context context = this;
    public static final int RequestPermissionCode = 1;

    CallApi.GetLogingCallBack mGetSDPListCallback = this;
    CallApi.getUserRegisterCallback mGetSDPListCallbackReg = this;

    CallBackInterface.userLogingStatesCallback mLogingStatesCallback = this;
    final CallBackInterface.userLogingStatesCallback mStatesCallback = this;

    @Inject
    CallApi mDataManager;

    @Inject
    ApiService mSicModul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {
        }
        // Example of a call to a native method

        if (checkPermission()) {
            Toast.makeText(MainActivity.this, "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
        } else {
            requestPermission();
        }

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);
        btnLinkTo = (Button) findViewById(R.id.btnLinkTo);


        //check  user current location request  permian
        if (ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }

        SetupClass.newInstance(context, "token");

        this.mSicModul = component.getApplicatonService();
        this.mDataManager = component.getClasesink();


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Helpers.showProgress(MainActivity.this);
                User mUser = new User();
                mUser.setPassword(inputPassword.getText().toString());
                mUser.setUsername(inputEmail.getText().toString());
                mDataManager.getServercal((CallApi.getUserRegisterCallback) mGetSDPListCallback, mUser);
            }
        });


        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Helpers.showProgress(MainActivity.this);
                User mUser = new User();
                mUser.setPassword(inputPassword.getText().toString());
                mUser.setUsername(inputEmail.getText().toString());
                mDataManager.register((CallApi.GetLogingCallBack) mGetSDPListCallbackReg, mUser);
            }
        });


    }

    @Override
    public void onSuccessuserLogingStatesCallback(int response, boolean logingstets) {
        Helpers.dismissProgress();
        Intent i = new Intent(this, BiometricsActivity.class);
        i.putExtra("BIOMETRICS_TYPE", response);
        i.putExtra("LOGIN_STATS", logingstets);
        i.putExtra("NOTIFICATION", false);
        startActivityForResult(i, 2);
    }

    @Override
    public void onSuccessBiometricsCompletedCallback(String response) {
        //loging scusess
        Helpers.dismissProgress();
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onFailedUserLogingStatesCallback(String message) {
        Helpers.dismissProgress();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2

        if (requestCode == 2) {
            if (data != null) {


                int message = data.getIntExtra("MESSAGE", -1);

                if (message == 1) {

                    mLogingStatesCallback.onSuccessBiometricsCompletedCallback("go hame");

                } else {

                    mLogingStatesCallback.onFailedUserLogingStatesCallback("login fail");
                }
            }
        }

    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(MainActivity.this, new String[]
                {
                        CAMERA, ACCESS_FINE_LOCATION,
                        WRITE_EXTERNAL_STORAGE
                }, RequestPermissionCode);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCode:

                if (grantResults.length > 0) {

                    boolean CameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadContactsPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locaion = grantResults[2] == PackageManager.PERMISSION_GRANTED;


                    if (CameraPermission && ReadContactsPermission) {

                        Toast.makeText(MainActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    public boolean checkPermission() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int TredPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);


        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onSuccessUserRegister(String sdpList) {
        Helpers.dismissProgress();
        MainAuthenticateClass.newInstance(MainActivity.this, sdpList, componentBabylone, mStatesCallback);
    }

    @Override
    public void onFailedUserRegister(String message) {
        Helpers.dismissProgress();
    }

    @Override
    public void onSuccessLogingCallBack(String sdpList) {
        Helpers.dismissProgress();

    }

    @Override
    public void onFailedLogingCallBack(String message) {
        Helpers.dismissProgress();
    }
}


