package com.sampathsampel.babylon.sampelapplication.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sampathsampel.babylon.sampelapplication.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        try {
            getSupportActionBar().hide();

        } catch (NullPointerException e) {
        }
    }
}
